import 'package:flutter/material.dart';
import 'package:flutter_application_2/pages/homepage.dart';
import 'package:flutter_application_2/pages/login_page.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(

      themeMode: ThemeMode.light,
      theme: ThemeData( primarySwatch: Colors.brown),
        
     
      darkTheme: ThemeData(
        brightness: Brightness.dark,

      ),
      routes: {
        "/":(context) => Loginpage(),
        "/login":(context) => Loginpage()
      },

    );
  }
}
